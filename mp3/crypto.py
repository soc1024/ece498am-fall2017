# -*- coding: utf-8 -*-
""" CS498AM -- Searchable Encryption -- crypto.py
"""

import base64
import os

import binascii

import struct

from Crypto.Cipher import AES
from Crypto.Hash import SHA256, HMAC

KEYSIZE = AES.block_size
KEYBITS = AES.block_size * 8



## str to and from long (128 bits)

def str_to_long128(x):
    r = 0
    t = struct.unpack(b"<IIII", x)
    for i in range(4):
        r += t[i] << (i * 32)
    return r

def long128_to_str(x):
    assert 0 <= x < 2**128
    t = []
    for i in range(4):
        t.append((x >> (i * 32) & 0xffffffff))
    s = struct.pack(b"<IIII", *t)
    return s

## randomness

# random bytes from os.urandom
def random_bytes(count):
    assert count > 0
    return os.urandom(count)

# uniformly random long with 128 bits
def random_long128():
    return str_to_long128(random_bytes(16))

# uniformly random long within bounds (at most 128 bits)
# uses rejection sampling
# note: min inclusive, max exclusive
def random_long(min_val_incl=0, max_val_excl=2**128):
    # Input: min, max
    # Output: a long in the range [min, max[
    assert min_val_incl >= 0 and max_val_excl <= 2**128 and min_val_incl < max_val_excl

    import math
    mask = 2**int(math.ceil(math.log(max_val_excl, 2))) - 1

    while True:
        x = random_long128() & mask
        if x >= min_val_incl and x < max_val_excl:
            break
    return x

# uniformly random key
def random_key(size=KEYSIZE):
    return random_bytes(size)
    
# randomly shuffle list using knuth-fischer-yates
def shuffle(x):
    assert type(x) == list
    
    def swap(x, i, j):
        tmp = x[i]
        x[i] = x[j]
        x[j] = tmp
        
    for i in xrange(0, len(x)-1):
        j = random_long(i, len(x))
        if i != j:
            swap(x, i, j)

## hash functions, PRFs, random oracles

# SHA256 digest
def sha2(x):
    return SHA256.new(x).digest()   

# hex of SHA256 digest
def sha2_hex_digest(x):
    return binascii.hexlify(sha2(x))
    
# truncated HMAC based on SHA256
def truncated_hmac(k, x):
    return HMAC.new(k, msg=x, digestmod=SHA256).digest()[:KEYSIZE]



## padding and encoding (b64) for encryption

# note: pad_char is chosen to not be among the b64 characters
def pad_it(data, pad=AES.block_size, pad_char=' '):
    return data + (pad - len(data) % pad) * pad_char
    
def unpad(data, pad_char=' '):
    return data.rstrip(pad_char)
    

# encode plaintext
def pt_encode(val):
    return pad_it(base64.b64encode(val))
    
# decode plaintext
def pt_decode(val):
    return unpad(base64.b64decode(val))
    
# encode ciphertext
def ct_encode(val):
    return base64.b64encode(val)
    
# decode ciphertext
def ct_decode(val):
    return base64.b64decode(val)

    
## IND-CPA encryption and decryption
    
"""
## Encrypt a string using AES-CBC.

    Inputs: 
        key: encryption key, 
        val: str to encrypt
    Outputs:
        obj: dict containing the corresponding ciphertext
        
    Note: this is to encrypt documents in a semantically secure way, not the index!
"""
def encrypt_str(key, val):
    iv = random_bytes(AES.block_size)
    cipher = AES.new(key, mode=AES.MODE_CBC, IV=iv)
    ct = cipher.encrypt(pt_encode(val))
    
    obj = {}
    obj['iv'] = base64.b64encode(iv)
    obj['ct'] = ct_encode(ct)
    
    return obj
    
"""
## Problem 0: Decrypt a dict obj encrypted with AES-CBC (5 points)
    This is the inverse function of encrypt_str()

    Inputs:
        key: encryption key,
        enc_obj: a dict containing the ciphertext and the IV
    
    Outputs:
        pt: the recovered plaintext string
        
    Hint: use ct_decode() and pt_encode() (provided above)
"""
def decrypt_str(key, enc_obj):
    assert type(enc_obj) is dict and 'iv' in enc_obj and 'ct' in enc_obj
    
    # TODO: your code goes here
    
    
## test
if __name__ == '__main__':
    key = random_key()
    document = "This is MP3 for CS/ECE498AM: Searchable Encryption!"
    
    doc_sha2 = sha2_hex_digest(document)

    enc_doc = encrypt_str(key, document)
    retrieved_doc = decrypt_str(key, enc_doc)
    
    assert document == retrieved_doc and document is not retrieved_doc
    assert sha2_hex_digest(retrieved_doc) == doc_sha2
    assert doc_sha2 == 'c9245352b2eebe296f8585b673dd7717c63c8b362ecd2ec68fe8f0c2deaaf35b' 
    
    print('Encryption test successful!')