# -*- coding: utf-8 -*-
""" CS498AM -- Searchable Encryption -- protocol.py

# This file tests the behavior of the basic and dynamic client and servers.
"""

import os
import utils
import crypto

import constructions

from client import BasicSSEClient, DynamicSSEClient
from server import BasicSSEServer, DynamicSSEServer


"""
## Run queries and return the results

For each query:
    1. the client computes the search token (and sends it to the server)
    2. the server searches the encrypted index and returns matching documents ids
    3. for each document: the client retrieves it from the server, 
                        decrypts it locally and retrives the content
                        
Outputs: 
    results: a dict associating each query with the list matching documents (contents)

"""
def run_queries(c, s, queries, docs_root, observe_fn=None):
    results = {}
    for query in queries:
        token = c.search_token(query)
        doc_ids = s.search(token)
            
        if observe_fn is not None:
            observe_fn(token, doc_ids) # adversary's observation
            
        docs = []
        for doc_id in doc_ids:
            # retrieve the document from the "server"
            enc_doc_fp = os.path.join(docs_root, '{}.json'.format(doc_id))
            enc_doc = utils.read_json(enc_doc_fp)
            
            # client decrypt the document
            doc = c.decrypt_document(enc_doc)
            docs.append(doc)
            
        results[query] = docs
    return results

"""
## Run the client and server protocol and return results

Works for both basic and dynamic clients/servers

1. Setup phase for the client and server
    - key material generation
    - index creates and encrypt the index and documents
    
2. Online phase
    - client queries server --- see run_queries()
    
3. (For dynamic protocol only) add new documents
    - client encrypts new documents
    - client computes and sends update tokens to server
    - server uses tokens to update (without decryption) the encrypted index
    - client queries server --- see run_queries()
                    
Outputs: 
    results: a dict associating each query with the list matching documents (contents)
    results2: (only for dynamic protocol) query results after new documents are added

"""
def run_protocol(workdir, data_dirfp, queries, dynamic=False, new_docs=None, observe_fn=None):
    # prepare a sub-directory in the workdir to store encrypted documents
    docs_root = os.path.join(workdir, 'docs_root')
    utils.ensure_exists(docs_root)
    
    # prepare a sub-directory in the workdir to store the encrypted index
    index_root = os.path.join(workdir, 'index_root')
    utils.ensure_exists(index_root)
    
    ### setup phase: initialization
    
    # create a new client
    ro = None
    if dynamic:
        ro = constructions.new_hmac_random_oracle()
        c = DynamicSSEClient(ro)
    else:
        c = BasicSSEClient()
    
    # generate key material for the client
    c.gen()
    
    # create encrypted index & encrypt documents
    c.create_index(data_dirfp, index_root, docs_root)
    
    # create a new server (we simulate the client sending it the encrypted docs and index)
    if dynamic:
        s = DynamicSSEServer(index_root, docs_root, ro=ro) # pass the same random oracle to the server
    else:
        s = BasicSSEServer(index_root, docs_root)

    ### query phase: client queries the server and gets the results
    results = run_queries(c, s, queries, docs_root, observe_fn=observe_fn)
    
    results2 = None
    if dynamic == True and new_docs is not None:
        for new_doc in new_docs:
            doc_fp, doc_id = new_doc
            
            # generate tokens for new doc
            tokens = c.add_document(docs_root, doc_fp, doc_id) 
            
            # add entries to index
            s.add_to_index(tokens)
            
        results2 = run_queries(c, s, queries, docs_root, observe_fn=observe_fn)
        
    return results, results2


# use sha256 digests top check query results
def check_res_sha2(docs, expected_sha2s):
    assert len(docs) == len(expected_sha2s)
    
    docs_sha2s = []
    for doc in docs:
        docs_sha2s.append(crypto.sha2_hex_digest(doc))

    docs_sha2s = set(docs_sha2s)
    for sha2 in expected_sha2s:
        assert sha2 in docs_sha2s
   
# expected sha256 hex digests for query: 'info'
info_expected_sha2 = ['1981256fbb963e3f8e8409fc24369d98f87a1c21f66c61a27f4f444d3032006c']

# expected sha256 hex digests for query: 'good'
good_expected_sha2 = ['653960b8164fe138944491c446fa964ec80b6d10818085a691f4e78ae1495f6e', \
                        'bf5f418efe3149b49b942dbbeac580d4b637de9ae7c1d999a1e2312d9e7c10c8', \
                        '096053ee4c54a55001dc00427d3f51882afd285605f3f93a32c6d6d91b97bda6', \
                        '5f7171467b2d549b341610098ac3d0a9c228ea15357c57e55f52e213179fdabd']         

# test output for the simple case    
def test_queries_simple(res, queries):
    assert len(res) == len(queries)
    assert len(res[queries[0]]) == 0
    assert len(res[queries[1]]) == 1
    assert len(res[queries[2]]) == 4
    
    check_res_sha2(res[queries[1]], info_expected_sha2)
    check_res_sha2(res[queries[2]], good_expected_sha2)
          
          
# test for the simple case (no docs added)        
def simple_test(workdir, data_dirfp, dynamic=False):
    queries = ['test','info','good']
    res, _ = run_protocol(workdir, data_dirfp, queries, dynamic)
    
    test_queries_simple(res, queries)    
    print 'simple protocol test (dynamic: {}) complete!'.format(dynamic)
    
# test output for the case with new docs added     
def test_queries_new(res, queries):
    assert len(res) == len(queries)
    assert len(res[queries[0]]) == 0
    assert len(res[queries[1]]) == 2
    assert len(res[queries[2]]) == 6
    
    new_info_expected_sha2 = info_expected_sha2 + ['9430767256f026a8f2ff3cfe268a0c2104c4ac024d2ef616c736b68effe5946c']
    
    new_good_expected_sha2 = good_expected_sha2 + ['9430767256f026a8f2ff3cfe268a0c2104c4ac024d2ef616c736b68effe5946c', \
                                '1ce57e40e7673b06e8c8b79ff390826e8e809cf1fded88f29c96e9a94e2c4f3d']

    check_res_sha2(res[queries[1]], new_info_expected_sha2)
    check_res_sha2(res[queries[2]], new_good_expected_sha2)
                
                
# test for the case with new docs added    
def new_docs_test(workdir, data_dirfp, new_docs_dirfp):
    queries = ['test','info','good']
    
    new_docs = []
    new_docs.append((os.path.join(new_docs_dirfp, 'new_doc1'), 100001))
    new_docs.append((os.path.join(new_docs_dirfp, 'new_doc2'), 100002))
    res, res2 = run_protocol(workdir, data_dirfp, queries, dynamic=True, new_docs=new_docs)
    
    test_queries_simple(res, queries) 
    test_queries_new(res2, queries)    
    
    print 'new docs protocol test complete!'
    

 ## run the tests               
if __name__ == '__main__':
    test_data_fp = os.path.join(os.path.join(os.getcwd(), 'data'), 'test')
    assert os.path.exists(test_data_fp) and os.path.isdir(test_data_fp), 'Can''t find test data!'
    test_workdir = os.path.join(os.getcwd(), 'test_workdir')
    
    import tempfile
    import shutil

    test_workdir = tempfile.mkdtemp()
    utils.test_with_cleanup(lambda: simple_test(test_workdir, test_data_fp), 
                      lambda: shutil.rmtree(test_workdir))
                      
    test_workdir = tempfile.mkdtemp()
    utils.test_with_cleanup(lambda: simple_test(test_workdir, test_data_fp, True), 
                      lambda: shutil.rmtree(test_workdir))
                      
                      
    test_workdir = tempfile.mkdtemp()
    test_new_docs_fp = os.path.join(os.path.join(os.getcwd(), 'data'), 'test_extras')
    utils.test_with_cleanup(lambda: new_docs_test(test_workdir, test_data_fp, test_new_docs_fp), 
                      lambda: shutil.rmtree(test_workdir))