# -*- coding: utf-8 -*-
""" CS498AM -- Searchable Encryption -- client.py

[1] Kamara, Seny, Charalampos Papamanthou, and Tom Roeder. 
    "Dynamic searchable symmetric encryption." ACM CCS, 2012.
    https://eprint.iacr.org/2012/530.pdf
"""

import os

import utils
import crypto

import constructions

__metaclass__ = type


"""
## Base class for the SSE client
"""
class SSEClient:
    def __init__(self):
        pass
    
    def gen(self):
        raise NotImplementedError()
        
    def encrypt_document(self, content_str):
        """ This function encrypts a document (a string) using the client's key.
        It returns a dict representation of the encrypted document. """
        raise NotImplementedError()
        
    def decrypt_document(self, enc_obj):
        """ This function decrypts an encrypted document (dict) using the client's key.
        It returns a string of the (plaintext) content. This is the inverse of encrypt_document(). """
        raise NotImplementedError()
        
    def init_index(self, index_root, num_docs, num_keywords, num_entries, num_free=1024):
        """ This function initializes the index data structure. """
        raise NotImplementedError()
        
    def save_index(self, index_root):
        """ This function saves the index data structure (to disk). """
        raise NotImplementedError()
        
    def process_keyword(self, index_root, keyword, docs_list):
        """ This function adds a keyword to the index data structure.
        It is meant to be used during the initialization phase. """
        raise NotImplementedError()
        
    def add_document(self, enc_docs_root, doc_fp, doc_id):
        """ This function adds a keyword to the index data structure.
        This is meant to provide the dynamic functionality: add documents after initialization.
        The function returns a sequence of tokens that the server uses to update the index. 
        (Only dynamic clients need to support this. """
        raise NotImplementedError()    

        
    def create_index(self, docs_root, enc_index_root, enc_docs_root):
        """ This function creates the index data structure.
        It call other index function to populate the index with the provided documents.
        Documents are retrieved from 'docs_root', stored encrypted in 'enc_docs_root'.
        The index is stored in 'enc_index_root'. """
        
        assert os.path.isdir(enc_docs_root), 'Encrypted docs root {} does not exist!'.format(enc_docs_root)
        assert os.path.isdir(enc_index_root), 'Encrypted index root {} does not exist!'.format(enc_index_root)
 
        assert os.path.isdir(docs_root), 'Documents root {} does not exist!'.format(docs_root)
        
        stopwords_fp = os.path.join(docs_root, 'stopwords.json')
        assert os.path.isfile(stopwords_fp), 'Stopwords file {} does not exist!'.format(stopwords_fp)
                        
        stopwords = utils.get_stopwords(stopwords_fp)
        self.stopwords = stopwords
        
        # get all documents
        docs_list = os.listdir(docs_root)      
        assert len(docs_list) > 0, 'Documents root {} is empty!'.format(docs_root)
        
        inverted_index = {}
        entries = 0
        # iterate over all documents one by one
        for doc_id in docs_list:
            doc_fp = os.path.join(docs_root, doc_id)
            if not os.path.isfile(doc_fp) or os.path.splitext(doc_fp)[-1] != '':
                continue
            
            doc_id = int(doc_id) # use it as an int
            
            doc_str = utils.read_text_file(doc_fp)
            
            # encrypt the document
            enc_doc_obj = self.encrypt_document(doc_str) 
            
            # store the encrypted object as a json file            
            enc_doc_fp = os.path.join(enc_docs_root, '{}.json'.format(doc_id))
            utils.write_json(enc_doc_fp, enc_doc_obj)
            
            # tokenize the document
            keywords = utils.tokenize(doc_str, stopwords=stopwords)
            
            for keyword in keywords:
                if keyword not in inverted_index:
                    inverted_index.setdefault(keyword, [])
                inverted_index[keyword].append(doc_id)
                entries += 1

        assert entries > 0, 'Empty index!'

        self.init_index(enc_index_root, len(docs_list), len(inverted_index), entries)

        # for every keyword in inverted index, incrementally build the encrypted index
        for keyword in inverted_index:
            docs_list = inverted_index[keyword]
            
            # process this keyword
            self.process_keyword(enc_index_root, keyword, docs_list)
        
        self.save_index(enc_index_root)
        
        return True     


        
"""
## Basic SSE client class (not dynamic)

Part 1.
"""
class BasicSSEClient(SSEClient):
    
    """
    ## Generate key material:
        - doc_key: to encrypt documents
        - token_prf: for search tokens
    """
    def gen(self):
        self.doc_key = crypto.random_key()
        self.token_prf = constructions.new_prf(crypto.random_key())
        
    def encrypt_document(self, content_str):
        return crypto.encrypt_str(self.doc_key, content_str)
        
    def decrypt_document(self, enc_obj):
        return crypto.decrypt_str(self.doc_key, enc_obj)
        
    def init_index(self, index_root, num_docs, num_keywords, num_entries, num_free=1024):
        pass # nothing to do
        
    def save_index(self, index_root):
        pass # nothing to do
        
    """
    ## Add the postings list for the keyword to the encrypted index.
    The postings list is stored as a json file.
    
    Note: this method is called once for every keyword encountered while creating the encrypted index
    
    Outputs: none
    """
    def process_keyword(self, index_root, keyword, docs_list):
        # compute the token
        token = self.token_prf(keyword)
        
        # corresponding filepath
        fp = constructions.get_table_entry(index_root, token)
        
        # create this index node and write it to file (JSON)
        obj = {'docs': docs_list}
        utils.write_json(fp, obj)
    
    """
    ## Compute the search token for the specified keyword (online phase) 
    """
    def search_token(self, keyword):
        token = self.token_prf(keyword)
        return token
        
"""
## Dynamic SSE client class
"""
class DynamicSSEClient(SSEClient):
    
    def __init__(self, ro):
        self.ro = ro
        
    def gen(self):
        self.doc_key = crypto.random_key()
        
        self.token_prf = constructions.new_prf(crypto.random_key())
        self.node_prf = constructions.new_prf(crypto.random_key())
        self.node_ro_prf = constructions.new_prf(crypto.random_key())
        
    def encrypt_document(self, content_str):
        return crypto.encrypt_str(self.doc_key, content_str)
        
    def decrypt_document(self, enc_obj):
        return crypto.decrypt_str(self.doc_key, enc_obj)
        
        
    def init_index(self, index_root, num_docs, num_keywords, num_entries, num_free=1024):
        max_nodes = num_entries + (1024 - (num_entries % 1024)) + num_free
        self.nodes = [crypto.random_long128() << 128 | crypto.random_long128() for i in xrange(0, max_nodes)]
        self.free_list = [i for i in xrange(0, max_nodes)]


    """
    ## Problem 2.a: implement process keyword (initialization phase) (25 points)
    
    Remark: the construction here follows Kamara et al. [1]
    
    The postings list for the keyword is a linked-list of nodes stored randomly in the nodes table.
    For each keyword, the (encrypted) address of the postings list (pointer to a node) must be stored in a JSON file.
    
    Note: this method is called once for every keyword encountered while creating the encrypted index
    
    Outputs: none
    
    Hint: must be consistent with search_token() *and* server.search()!
    """
    def process_keyword(self, index_root, keyword, docs_list):                    
        # populate the nodes table (embed the postings list) 
        #
        # hint: consider processing linked-list nodes in reverse order!
        # TODO: your code goes here
         
        # compute the encrypted address 'enc_addr' of the head node.    
        # and update the index table accordingly (JSON file)
        # TODO: your code goes here
        
        
    def save_index(self, index_root):       
        free_fp = constructions.get_table_entry(index_root, 'free')
        utils.list_to_file(free_fp, self.free_list)

        nodes_fp = os.path.join(index_root, 'nodes.data')
        utils.list_to_file(nodes_fp, self.nodes)

    """
    ## Problem 2.b: compute the search token for the specified keyword (online phase) (5 points)
    
    Inputs:
        keyword: the keyword
        
    Outputs:
        t1, t2, t3: the search token(s)
    
    Must be consistent with process_keyword()
    """
    def search_token(self, keyword):
        # TODO: your code goes here
        
    """
    ## Problem 2.d: add a document, i.e., create tokens for server to update the index for the specified document (online phase) (10 points)
    
    Inputs:
        enc_docs_root: the directory where to store encrypted documents
        doc_fp: the new document to add
        doc_id: the new document identifier
        
    Outputs:
        tokens: list of update tokens, each is a tuple of the form (t1, t2, t3v, tr)
    
    Must be consistent with server.add_to_index()
    """
    def add_document(self, enc_docs_root, doc_fp, doc_id):
        assert os.path.isfile(doc_fp)
        # read the document
        doc_str = utils.read_text_file(doc_fp)
        
        # encrypt the document
        enc_doc_obj = self.encrypt_document(doc_str) 
        
        # store the encrypted object as a json file            
        enc_doc_fp = os.path.join(enc_docs_root, '{}.json'.format(doc_id))
        utils.write_json(enc_doc_fp, enc_doc_obj)
        
        # tokenize the document
        keywords = utils.tokenize(doc_str, stopwords=self.stopwords)
        assert len(keywords) > 0
        
        # produce the tokens to be sent to the server (these will be used to update the index)
        tokens = []        
        for keyword in keywords:            
            # compute the 3 tokens t1, t2, t3 associated with keyword
            
            # TODO: your code goes here
            
            # compute the 3rd token value (t3v) and also the 4th value tr
            # so the server can homomorphically update the index
            # you can use: crypto.format_node() and crypto.long128_to_str() and crypto.str_to_long128()
            
            # TODO: your code goes here
            
            tokens.append((t1,t2,t3v,tr))

        return tokens
        
    """
    ## (Bonus) Problem 2.f: implement deletion functionality as described in [1]. (10 points)
    
    This requires implementing a delete_document() for the client and delete_from_index() for the server
    as well as changes to the index data structures.
    
    Make sure your solution still passes the tests defined in protocol.py
    """