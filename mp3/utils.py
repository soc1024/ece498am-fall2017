# -*- coding: utf-8 -*-
""" CS498AM -- Searchable Encryption -- utils.py
"""

import json
import re
import os

## os / paths
def ensure_exists(dir_fp):
    if not os.path.exists(dir_fp):
        os.makedirs(dir_fp)

## json stuff

# write a dict or a json string to file
def write_json(fp, obj_or_str):
    json_obj = obj_or_str
    if isinstance(obj_or_str, basestring):
        json_obj = json.loads(obj_or_str)
        
    with open(fp, 'w') as f:
        json.dump(json_obj, f, sort_keys=True, indent=4, separators=(',', ': '))    
    
# read a json obj from file and return a dict
def read_json(fp):
    with open(fp, 'r') as f:
        obj = json.load(f)
    return obj
    
    
## text files
    
# read a text file; return a string
def read_text_file(fp):
    with open(fp, 'r') as f:
        contents = f.read()
    return contents
    
# write a string to a text file
def write_text_file(fp, contents):
    with open(fp, 'w') as f:
        f.write(contents)
        
# write a list of longs to a text file    
def list_to_file(fp, arr):
    with open(fp, 'w') as f:
        for v in arr:
            f.write(str(v) +"\n")

# read a text file containing a list of long and return the list
def list_from_file_long(fp):
    with open(fp, 'r') as f:
        content = f.readlines()
    return [long(x.strip()) for x in content] 
    
    
## parsing & stopwords
    
# retrieve stopwords from a file (JSON format)
def get_stopwords(fp):
    """ This function retrieves the stopword (stored in a JSON file). """
    stopwords_json = read_json(fp)
    stopwords_main = stopwords_json['list'] # main stopwords
    
    stopwords_extras = []
    if 'extras' in stopwords_json:
        stopwords_extras = stopwords_json['extras'] # extra stopwords specific to corpus
        
    return stopwords_main + stopwords_extras
    

# tokenize a document (a str) into tokens, removing stopwords encountered; returns a list of tokens
def tokenize(target, stopwords=None):
    if stopwords is None:
        stopwords = []
    
    # extract words within target
    words = re.findall(r"[\w']+", target)
    
    # lowercase
    lc_words = map(lambda x:x.lower(), words)
    
    # remove duplicates and stopwords
    tokens = list(set(lc_words).difference(stopwords)) 
    return tokens
    
## tests
    
# run test_fn and cleanup_fn no matter what
def test_with_cleanup(test_fn, cleanup_fn):
    try:
        test_fn()
    except AssertionError:
        raise 
    finally:
        cleanup_fn()
