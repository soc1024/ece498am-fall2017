# -*- coding: utf-8 -*-
""" CS498AM -- Searchable Encryption -- server.py

[1] Kamara, Seny, Charalampos Papamanthou, and Tom Roeder. 
    "Dynamic searchable symmetric encryption." ACM CCS, 2012.
    https://eprint.iacr.org/2012/530.pdf
"""

import os

import utils
import crypto

import constructions

__metaclass__ = type

"""
## Base class for the SSE server
"""
class SSEServer:
    
    def __init__(self, index_root, docs_root):
        self.index_root = index_root
        self.docs_root = docs_root
                
    def search(self, token):
        """ This function performs a search over the encrypted index using 'token'.
        It returns a list of matching document ids (to the keyword associated by 'token')."""
        raise NotImplementedError()
        
    def add_to_index(self, tokens):
        """ This function updates the encrypted index with a new document using the provided tokens.
        This functionality is only supported by dynamic servers."""
        raise NotImplementedError()

"""
## Basic SSE server (not dynamic)
"""
class BasicSSEServer(SSEServer):
    
    """
    ## Problem 1.a: implement search (5 points)
    
    Retrieve the doc id list for the given token.
    
    Inputs:
        - token: search token from the client
    
    Outputs:
        - list of matching document ids
    
    Note: returns an empty list if the token does not match any encrypted index (table) entry
    """
    def search(self, token):
        # lookup the table entry and retrieve the postings list
        # must return an empty list if there are no matching documents
        #
        # hint: must be consistent with the client and search_token()
    
        # TODO: your code goes here

"""
## Dynamic SSE server
"""
class DynamicSSEServer(SSEServer):
    def __init__(self, index_root, docs_root, ro):
        super(DynamicSSEServer, self).__init__(index_root, docs_root)  
        
        self.ro = ro
        
        self.nodes_fp = os.path.join(index_root, 'nodes.data')
        assert os.path.isfile(self.nodes_fp), 'Invalid index: missing nodes file!'
        
        self.nodes = utils.list_from_file_long(self.nodes_fp)
        
        self.free_fp = constructions.get_table_entry(index_root, 'free')
        assert os.path.isfile(self.free_fp), 'Invalid index: missing free list file!'
        self.free_list = utils.list_from_file_long(self.free_fp)
        
    
    """
    ## Problem 2.c: implement search (online phase) (10 points)
        
    Given a token, search the encrypted index and retrieve the list of matching document identifiers
    
    Hint: you may use lookup_node() to walk the linked-list
    
    Note: must return an empty list if there are no matching documents   
    
    Inputs:
        token: a token of the form (t1, t2, t3) 
        
    Outputs:
        docs: a list of matching document identifiers

    Hint: must be consistent with search_token()
    """
    def search(self, token):
        t1, t2, t3 = token
        
        # TODO: your code goes here
        
        
    def lookup_node(self, node_ptr, tok):
        """ This function looks up the next node in the list. It uses 'tok' to decrypt. """
        assert node_ptr > 0
        assert node_ptr <= len(self.nodes)
        enc_node = self.nodes[node_ptr-1]

        nc, rv = constructions.parse_enc_node(enc_node)
        node_val = nc ^ crypto.str_to_long128(self.ro(tok, crypto.long128_to_str(rv)))
        
        doc_id, next_ptr = constructions.parse_node(node_val)
        return doc_id, next_ptr
        
    
    """
    ## Problem 2.e: implement homomorphic pointer update for add_to_index (online phase) (10 points)
        
    Given a list of update tokens, update the encrypted index
    
    Note: your code only needs to create encrypted nodes using the homomorphic pointer update. (The rest is provided.)   
    
    Inputs:
        tokens: list of update tokens, each is a tuple of the form (t1, t2, t3v, ri)
        
    Outputs: none

    Hint: must be consistent with client.add_document()
    """
    def add_to_index(self, tokens):
        for token in tokens:
            t1, t2, t3, rv = token
            
            # get table entry corresponding to keyword
            fp = constructions.get_table_entry(self.index_root, t1)
            
            if os.path.isfile(fp):
                obj = utils.read_json(fp)
                
                assert 'node_ptr' in obj, 'Invalid table entry {}!'.format(fp)
                node_ptr = obj['node_ptr'] ^ crypto.str_to_long128(t2)
            else:
                node_ptr = 0
            
            # get next free node
            new_node_addr = self.free_list.pop()
            
            # create the new node (use homomorphic pointer update) 
            # and write it encrypted to self.nodes[new_node_addr]
            # TODO: your code goes here
        
            # update the new table entry
            enc_addr = (new_node_addr+1) ^ crypto.str_to_long128(t2)
            obj = {'node_ptr': enc_addr}
            utils.write_json(fp, obj)
        
        utils.list_to_file(self.free_fp, self.free_list) # commit free list
        utils.list_to_file(self.nodes_fp, self.nodes) # commit nodes