# -*- coding: utf-8 -*-

""" CS498AM -- Searchable Encryption -- attack.py 

Zhang, Yupeng, Jonathan Katz, and Charalampos Papamanthou. 
"All Your Queries Are Belong to Us: The Power of File-Injection Attacks on Searchable Encryption." 
USENIX Security 2016
http://eprint.iacr.org/2016/172.pdf
"""

import os
import math

import utils
import crypto

import protocol

## attack

## Uses a hash function to map a keyword to a deterministic set of bits
def keyword_to_bits(keyword, length=24):
    assert length <= 256
    
    dig = crypto.sha2(keyword)
    digv = crypto.str_to_long128(dig[:16]) & (2**length - 1)
    
    bits = []
    for b in xrange(0, length):
        mask = 0x1 << b
        if (digv & mask) > 0:
            bits.append(b)
            
    return bits
    
"""
## Problem 3.a: implement inject documents (15 points)


Inputs:
    data_dirfp: directory (str) where to inject the documents
    keywords: list of all keywords in the corpus
    num_docs: number of documents to inject

Outputs:
    injected_docs_map: a dict mapping each injected doc id (int) to a list of keywords

Note: must be consistent with injection_attack_recovery()
"""    
def inject_documents(data_dirfp, keywords, num_docs=32):
    injected_docs_map = {}
    
    assert 2*int(math.ceil(math.log(len(keywords), 2))) < num_docs
    
   
    # Inject num_docs maliciously crafted files (as individual files in data_dirfp)
    # Use injected_docs_map to keep track of which keywords you put in which file
    # 
    # Hint: injected_docs_map should map each injected doc ids to the list of keywords in the injected file.
    #    
    # Note: you may use the provided keyword_to_bits() method in your implementation
    
    # TODO: your code goes here
        
    return injected_docs_map
    
"""
## Problem 3.b: implement injection attack recovery (15 points)

Inputs:
    trace: the server's observed trace, a list of tuples of the form (token, res)
    injected_docs_map: a dict mapping each injected doc id (int) to a list of keywords
    num_docs: number of documents to inject

Note: must be consistent with inject_documents()
"""   
def injection_attack_recovery(trace, injected_docs_map, num_docs=32):
    
    known_queries = {}
    # TODO: your code goes here
    
    # for each token in the trace and recover the associated keyword
    # known_queries must be a dictionary mapping each (observed) token to its correct keyword
    
        
    return known_queries, {}


## evaluation, indexing and test

# extract all the keywords from a corpus
def index_keywords(data_dirfp):
    # get all documents
    docs_list = os.listdir(data_dirfp)      
    assert len(docs_list) > 0, 'Documents root {} is empty!'.format(data_dirfp)

    stopwords_fp = os.path.join(data_dirfp, 'stopwords.json')
    assert os.path.isfile(stopwords_fp), 'Stopwords file {} does not exist!'.format(stopwords_fp)
                    
    stopwords = utils.get_stopwords(stopwords_fp)
            
    all_keywords = set()
    # iterate over all documents one by one
    for doc_id in docs_list:
        doc_fp = os.path.join(data_dirfp, doc_id)
        if not os.path.isfile(doc_fp) or os.path.splitext(doc_fp)[-1] != '':
            continue
        
        doc_id = int(doc_id) # use it as an int
        
        doc_str = utils.read_text_file(doc_fp)
        
        # tokenize the document
        keywords = utils.tokenize(doc_str, stopwords=stopwords)
        
        all_keywords = all_keywords.union(keywords)

        
    return list(all_keywords)
    



# evaluate attack recovery rates
def evaluate_attack(true_map, known_queries, unknown_queries):
    assert len(known_queries) + len(unknown_queries) == len(true_map)
        
    correct = 0.0
    for token, keyword in known_queries.iteritems():
        true_keyword = true_map[token]
        if keyword == true_keyword:
            correct += 1.0
    incorrect = len(known_queries) - correct
    
    recov_rate = correct / len(true_map)
    false_recov = incorrect / len(true_map)
    unknown = float(len(unknown_queries)) / len(true_map)
    
    print('[Attack results] correct recovery: {:.2f}%, incorrect: {:.2f}%, unknown: {:.2f}%'.format(100*recov_rate, 100*false_recov, 100*unknown))


# observe queries and record a trace (from server's perspective)
def observe_query(trace, query, res):
    trace.append((query, res))

# setup for attack and select random queries
def prep_attack(data_dirfp, trace, num_queries=150):
    observe_fn = lambda q, r: observe_query(trace, q, r)
    
    keywords_list = index_keywords(data_dirfp)
    crypto.shuffle(keywords_list)
    num_queries = num_queries if len(keywords_list) > num_queries else len(keywords_list)
    queries = keywords_list[:num_queries]
    
    return observe_fn, keywords_list, queries
    
    
# retrieve the 'true' token to keyword mapping
def get_queries_map(trace, queries):
    true_map = {}
    for i, (token, res) in enumerate(trace):
        if token not in true_map:
            true_map[token] = queries[i]
    return true_map

    
# test the injection attack using random queries
def test_injection_attack(workdir, data_dirfp):
    print 'Testing injection attack...'
    
    # setup
    trace = []
    observe_fn, keywords, queries = prep_attack(data_dirfp, trace)
    
    num_injected_docs = int(2*math.ceil(math.log(len(keywords), 2.0)))+2
    
    # inject the documents
    injected_docs_map = inject_documents(data_dirfp, keywords, num_docs=num_injected_docs)
    
    # run the protocol
    res, _ = protocol.run_protocol(workdir, data_dirfp, queries, dynamic=False, observe_fn=observe_fn)
    
    # perform the attack
    known_queries, unknown_queries = injection_attack_recovery(trace, injected_docs_map, num_docs=num_injected_docs)    
    
    # compute the true token <-> keyword mapping
    true_map = get_queries_map(trace, queries)
    
    # evaluate
    evaluate_attack(true_map, known_queries, unknown_queries)
    
    print 'Injection attack test complete!'
    
    
if __name__ == '__main__':
    test_data_fp = os.path.join(os.path.join(os.getcwd(), 'data'), 'test')
    assert os.path.exists(test_data_fp) and os.path.isdir(test_data_fp), 'Can''t find test data!'
    test_workdir = os.path.join(os.getcwd(), 'test_workdir')
    
    import tempfile
    import shutil

    test_workdir = tempfile.mkdtemp()
    test_tmp_data_fp = os.path.join(tempfile.mkdtemp(), 'docs_root')
    
    # copy the contents
    shutil.copytree(test_data_fp, test_tmp_data_fp)
    
    utils.test_with_cleanup(lambda: test_injection_attack(test_workdir, test_tmp_data_fp), 
                      lambda: [shutil.rmtree(test_workdir), shutil.rmtree(test_tmp_data_fp)])
                      