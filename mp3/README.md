Machine Problem 3: Searchable Encryption
===================================
This is the third machine problem for ECE/S 498 AM Applied Cryptography at the University of Illinois at Urbana-Champaign. http://soc1024.ece.illinois.edu/teaching/ece498am/fall2017/

In this assignment you will implement parts of two Searchable Encryption protocols as well as a document injection attack.

This repository includes a PDF handout, `handout.pdf`, which provides details about this assignment and explains the format of the code.

As usual, the provided skeleton code include `#TODO` regions that you must fill out to complete this assignment. The number of points associated with each portion is given in the file.

Submitting your solution
------------------------

To submit your solution:
- The due date is (**SEE THE COURSE WEBSITE**)
- You must upload your `mp3` folder as a zip file to Piazza
- The upload must be marked "visible to Instructors"
- The `mp3` folder must contain a text file `report.txt`, which must include a short english-language narrative explaining:
- your net id
- what parts you finished, attempted, couldn't figure out
- any parts you found interesting, challenging, questions you have
- length can be one paragraph, or several... this is not an essay, but it may be used to justify partial credit
- **No partners are allowed for this machine problem.** You must do your own work on this MP.
- Only modify the `crypto.py`, `client.py`, `server.py`, and `attack.py` files, since that is the only code we'll check'
- Each script must run (and be able to be imported) without throwing exceptions
- You'll get points for every one of the included tests that passes (though the tests in the file are not comprehensive)

Instructions for testing your solution
==================================
## Problem 0:
```
python crypto.py
```

## Problems 1 and 2:
```
python protocol.py
```

## Problem 3:
```
python attack.py
```
