# -*- coding: utf-8 -*-
""" CS498AM -- Searchable Encryption -- constructions.py
"""

import os
import binascii

import crypto

## PRFs and random oracles (implemented to be consistent with Kamara et al [1])
    
# random oracle based on HMAC
def new_hmac_random_oracle():
    rk = crypto.random_key()
    return lambda k, r: crypto.truncated_hmac(crypto.sha2(k+rk), r)

# PRF instantiated with the  providedkey
def new_prf(k):
    return lambda x: crypto.truncated_hmac(k, x) 
    
    
## index manipulation (used in both basic and dynamic protocols)
## note: the index table is implemented using the filesystem (one file per entry)  
    
# return the filepath corresponding to the given token
# exception for 'free' which indicates a file containing a list of free nodes (for the dynamic scheme only)
def get_table_entry(root, token):
    tk = 'free' if token == 'free' else binascii.hexlify(token)
    return os.path.join(root, '{}.json'.format(tk))


## manipulation of node data structure of Kamara et al [1]
## a node is 256 bits and has the following structure:
## (enc_part, ri), where ri is a random 128bit long 
## enc_part = (doc_id, next_addr) XOR PRF output(ri)
## each of doc_id and next_addr are 64 bits

# parse an encrypted node (256 bits) into its encrypted part (128) and the random value rv (128)
def parse_enc_node(enc_node):
    r_mask = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    rv = enc_node & r_mask
    enc_part = enc_node >> 128
    return enc_part, rv
    
# format an encrypted part and random value into an encrypted node (inverse of parse_enc_node)
def format_enc_node(enc_part, rv):
    return enc_part << 128 | rv
    
# parse the inner node tuple (doc_id, next_addr)
def parse_node(node_val):
    return node_val >> 64, node_val & 0xFFFFFFFFFFFFFFFF
    
# format (doc_id, next_addr) into an inner node tuple (inverse of parse_node)
def format_node(doc_id, next_node_addr):
    return doc_id << 64 | (next_node_addr & 0xFFFFFFFFFFFFFFFF)