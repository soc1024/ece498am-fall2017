"""
## Handout 1: Programming with Polynomials and Lagrange Interpolation
"""
import sys
sys.path += ['elliptic-curves-finite-fields']
from finitefield.finitefield import FiniteField
from finitefield.polynomial import polynomialsOver
from finitefield.euclidean import extendedEuclideanAlgorithm
from elliptic import GeneralizedEllipticCurve, Point, Ideal
import elliptic
import os
import random


"""
## Polynomials over an example finite field Fp(71)
"""

Fp = FiniteField(71,1) #
Poly = polynomialsOver(Fp)

p1 = Poly([1,2,3,4,5])
print p1
# 1 + 2 t + 3 t^2 + 4 t^3 + 5 t^5 

# Evaluating a polynomial (this should be added as __call__!)
def eval_poly(f, x):
    assert type(x) is f.field
    y = f.field(0)
    for (degree,coeff) in enumerate(f):
        y += coeff * (x ** degree)
    return y

"""
## Generate a random polynomial
"""

def random_poly_with_intercept(s, k, Poly=Poly):
    # Returns a degree-k polynomial f
    # such that f(0) = 
    coeffs = [None] * (k+1)
    coeffs[0] = Poly.field(s)
    for i in range(1,k+1):
        coeffs[i] = Poly.field(random.randint(0,Poly.field.p-1))
    return Poly(coeffs)


"""
## Shamir's Secret Sharing
"""
def secret_share(s, k, n, Poly=Poly):
    assert type(k) is type(n) is int
    assert 2 <= k <= n
    f = random_poly_with_intercept(s, k-1)
    print "The secret value is f(0) = ", eval_poly(f, Poly.field(0))

    print "The polynomial is:", f

    s = [None] * n
    for i in range(n):
        s[i] = (i+1, eval_poly(f, Fp(i+1)))
        print "(%d, f(%d) = %s)" % (i+1, i+1, s[i][1])
    return s

# Example
# shares = secret_share(Fp(15), 3,5)

"""
## Compute the Lagrange coefficients  p[i](x)
   p[i](x) = product[over j != i] of (x - x[j])/(x[i] - x[j])
 
"""

def lagrange(S, i, n, Poly=Poly):
    # 
    # Assert S is a subset of range(0,self.l)
    assert type(S) is set
    assert S.issubset(range(1,n+1))
    S = sorted(S)

    x = Poly([0,1]) # This is the polynomial f(x) = x
    ONE = Poly([1]) # This is the polynomial f(x) = 1
    
    assert i in S
    assert 1 <= i <= n
    mul = lambda a,b: a*b
    num = reduce(mul, [x - j  for j in S if j != i], ONE)
    den = reduce(mul, [i - j  for j in S if j != i], Fp(1))
    return num / den

def decode_subset(shares, Poly=Poly):
    
    # S is a subset of indices from 1,2,...,k+1
    n = len(shares)
    S = [share[0] for share in shares if share is not None]

    f = 0
    print '----------'
    print 'Lagrange Coefficients'
    for share in shares:
        if share is None: continue  # skip the Nones
        i, yi = share
        pi = lagrange(set(S), i, n, Poly)
        print "p[%d](x):" % (i), pi
        f += pi * yi

    print '----------'

    print 'f(x) = sum( p[i](x) * y[i] ) = ', f


def try_decryption(shares, S):
    assert len(set(S)) == len(S) # No duplicates
    S = set(S)
    assert S.issubset(range(1,len(shares)+1)) # Valid indexes

    shares_subset = list(shares)  # Make a copy
    for i in range(1,len(shares)+1):
        if not i in S:
            shares_subset[i-1] = None
    print 'Trying to decrypt with the following subset:'
    print shares_subset

    decode_subset(shares_subset)
